FROM openjdk:11
EXPOSE 8080
ADD target/demo-aws.jar demo-aws.jar
ENTRYPOINT [ "java", "-jar" , "/demo-aws.jar" ]